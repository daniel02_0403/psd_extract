#!/usr/bin/env python

import sys
import json
import psd_tools
from psd_tools import PSDImage
from psd_tools.user_api.psd_image import merge_layers
from array import array

items = []

def main():
    if len(sys.argv) < 2:
        sys.exit('one argument needed: the path to the psd file')
        
    extract_psd(sys.argv[1])

def extract_psd(psd_file):
    psd = PSDImage.load(psd_file)
    
    for layer in psd.layers:
        if proceed_extract(layer):
            extract_layer(layer)
    
    write_json_file()

def extract_layer(layer):
    if isinstance(layer, psd_tools.Group):
        extract_group(layer)
    else:
        extract_png(layer)
        
def extract_group(group_layer):
    layers = []
    
    for layer in group_layer.layers:
        if proceed_extract(layer):
            extract_png(layer)
        layers.append(layer)
        
    name = get_layer_name(group_layer) + '.png'
    img_group = merge_layers(layers)
    img_group.save(name)
    items.append(create_json_item(name, get_layer_tags(group_layer)))
    
def extract_png(layer):
    name = get_layer_name(layer) + '.png'
    img = layer.as_PIL()
    img.save(name)
    items.append(create_json_item(name, get_layer_tags(layer)))
    
def proceed_extract(layer):
    if layer_has_tags(layer):
        if 'ignoremule' in map(str.lower, get_layer_tags(layer)):
            return False
        else:
            return True
    else:
        return False
    
def layer_has_tags(layer):
    splt = layer.name.split('#', 1)
    if len(splt) > 1:
        if splt[0] == '':
            return False
        else:
            return True
    else:
        return False
    
def get_layer_name(layer):
    return layer.name.split('#', 1)[0].strip(' \t\n\r')
    
def get_layer_tags(layer):
    return [item.strip(' \t\n\r') for item in layer.name.split('#', 1)[1].split('#')]

def create_json_item(image_name, tags):
    return {'image_name': image_name, 'tags': tags}

def write_json_file():
    json_file = open('assets.json', 'w')
    json_items = json.dumps(items, indent = 4, separators = (',', ':'))
    json_file.write(json_items)
        
if __name__ == "__main__":
    main()