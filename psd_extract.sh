#!/bin/bash

python_env=$1
python_script=$2
psd_file=$3

function error_exit
{
    echo
    echo "$1" 1>&2
    exit 1
}

source $python_env
python $python_script $psd_file

if [ "$?" != "0" ]; then
    error_exit "Something went wrong!"
fi